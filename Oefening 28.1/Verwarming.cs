﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._1
{
    internal class Verwarming
    {
        private double _graden = 0;

        public double Graden
        {
            get { return _graden; }
            set { _graden = value; }

           
        }
        public double inFarenheit() { return (Graden * 1.8) +32; }



        public bool Power { get; set; } = false;
    }
}
