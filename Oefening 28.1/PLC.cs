﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._1
{
    internal class PLC
    {
        public Lichten SalonLichten { get; set; } = new Lichten();
        public Verwarming Chauffage { get; set; } = new Verwarming();
        public void DoeLichtenAan() 
        {
            this.SalonLichten.Power = true;
        }
        public void DoeLichtenUit()
        {
            this.SalonLichten.Power = false;
        }
        public void ZetVerwarmingAf()
        {
            this.Chauffage.Power = false;
        }
        public void ZetVerwarmingOp()
        {
            this.Chauffage.Power = true;
        }
        public void PasTemperatuurAan(double graden)
        {
            this.Chauffage.Graden = graden;
        }
    }
}
