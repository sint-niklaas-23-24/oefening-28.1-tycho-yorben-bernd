﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Oefening_28._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        PLC pLC = new PLC();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            slideTemperatuur.Value = 0;
            slideTemperatuur.IsEnabled = false;
            RefreshTempData();
        }

        private void btnAanUit_Checked(object sender, RoutedEventArgs e)
        {
            imgAfbeelding.Source = new BitmapImage(new Uri(@"Images\Light On.JPG", UriKind.Relative));
            pLC.DoeLichtenAan();
        }

        private void btnAanUit_Unchecked(object sender, RoutedEventArgs e)
        {
            imgAfbeelding.Source = new BitmapImage(new Uri(@"Images\Light Off.JPG", UriKind.Relative));
            pLC.DoeLichtenUit();
        }

        private void slideTemperatuur_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            RefreshTempData();
        }

        private void btnOmhoog_Click(object sender, RoutedEventArgs e)
        {
            if (pLC.Chauffage.Power) slideTemperatuur.Value++;
        }

        private void btnOmlaag_Click(object sender, RoutedEventArgs e)
        {
            if (pLC.Chauffage.Power) slideTemperatuur.Value--;
        }

        private void toggleAanUitTemperatuur_Checked(object sender, RoutedEventArgs e)
        {
            pLC.ZetVerwarmingOp();
            slideTemperatuur.IsEnabled = true;
        }

        private void toggleAanUitTemperatuur_Unchecked(object sender, RoutedEventArgs e)
        {
            slideTemperatuur.IsEnabled = false;
            pLC.ZetVerwarmingAf();
        }

        private void RefreshTempData()
        {
            pLC.PasTemperatuurAan((double)slideTemperatuur.Value);
            txtTemp.Text = $"{pLC.Chauffage.Graden.ToString("0.00")}°C" + Environment.NewLine + $"{pLC.Chauffage.inFarenheit().ToString("0.00")}°F";
        }
    }
}
